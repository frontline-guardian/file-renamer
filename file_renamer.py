import argparse
import random
import os

WORD_LIST_PATH = os.path.join(".", "filenamelist.txt")
WORD_LIST_NO_NEWLINES = []
SEPARATORS = [" ", "_", "-"]

with open(WORD_LIST_PATH, "r") as f:
    FILE_NAMES = f.readlines()

for fn in FILE_NAMES:
    WORD_LIST_NO_NEWLINES.append(fn.strip())


def get_new_name(current_name, extension_prob, separator):
    chance = random.random()
    if chance <= extension_prob:
        new_name = "%s%s%s" % (current_name, separator, random.choice(WORD_LIST_NO_NEWLINES))
        return get_new_name(new_name, extension_prob / 2.0, separator)
    else:
        return current_name[1:]


def main():
    parser = argparse.ArgumentParser(description="User Space Document Renamer")
    parser.add_argument("--user_space", type=int, help="User Space Number", default=0)
    args = parser.parse_args()

    overall_path = os.path.join(".", "spaces", str(args.user_space))

    # This code needs refactoring but it works for now
    for root, dirs, files in os.walk(overall_path):
        sep = random.choice(SEPARATORS)
        for file in files:
            original = os.path.join(root, file)
            ext = os.path.splitext(original)[1]
            new_name = get_new_name("", 1.0, sep)
            new_path = os.path.join(root, "%s%s" % (new_name, ext))

            # Rename files
            success = False
            while not success:
                # Keep trying until successful
                try:
                    os.rename(original, new_path)
                    success = True
                except FileExistsError:
                    # File already exists so pick a new name and try again
                    new_name = get_new_name("", 1.0, sep)
                    new_path = os.path.join(root, "%s%s" % (new_name, ext))
                    pass

main()

